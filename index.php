<?php
///////////////////////////////
//
// new.php
//
//
//	for improvement purpose
//
//

?>
<!DOCTYPE html>
<html lang="en-TH">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<link rel="icon" href="data:image/x-icon;," type="image/x-icon">
	<link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon">  
	
	<script src="/vdo_control/jquery/jquery.min.js"></script>
	<link rel="stylesheet" href="/vdo_control/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="/vdo_control/bootstrap/css/bootstrap-theme.min.css" crossorigin="anonymous">
	<link rel="stylesheet" href="/vdo_control/bootstrap/css/animate.css" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="/vdo_control/bootstrap/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<script src="/vdo_control/bootstrap/js/bootbox.min.js" crossorigin="anonymous"></script>
	<script src="/vdo_control/bootstrap/js/bootstrap-notify.min.js" crossorigin="anonymous"></script>
	<script src="/vdo_control/asset/js/Queue.src.js?ver=<?php echo time(); ?>"></script>
	<script src="/vdo_control/asset/js/subject.js?ver=<?php echo time(); ?>"></script>
	<script src="/vdo_control/asset/js/test.js?ver=<?php echo time(); ?>"></script>
	<script src="/vdo_control/asset/js/vdo.js?ver=<?php echo time(); ?>"></script>
	<title>คุมกล้องไอคิวพลัส</title>
</head>
<body>
	<center>
		<div style="margin-top:5px;" class="div_container">
			<select class="form-control" id="teacher" disabled>
				<option value="not_select">เลือกครู:---</option>
				<option value="fern">พี่เฟิร์น</option>
				<option value="kong">พี่ก้อง</option>
				<option value="tom">ต้อม</option>
			</select>
			<select class="form-control" id="subject" disabled>
				<option value="not_select">เลือกวิชา:---</option>
			</select>
			<!-- <input type="text" placeholder="หมายเหตุ (กรณีมีเหตุการณ์พิเศษ เช่น สอบ)" class="form-control" id="remark" /> -->
		</div>
		<div class="report div_container">
			<div class="btn-group btn-group-justified" role="group">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="grn_camera_status_btn"><p class="status_text">สถานะกล้อง</p><p>ห้องเขียว</p></button>
					<button type="button" class="btn btn-default" id="grn_camera_extend_status_btn" style="display: none;"><p class="status_text">สถานะกล้อง</p><p>ห้องเขียว Extend</p></button>
				</div>
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="grn_rec_status_btn"><p>ห้องเขียวกำลังอัด</p><p class="duration">&nbsp;</p></button>
					<button type="button" class="btn btn-default" id="grn_rec_extend_status_btn" style="display: none;"><p>ห้องเขียว Extend กำลังอัด</p><p class="duration">&nbsp;</p></button>
				</div>
			</div>
			<div><span> </span></div>
		</div>
		<div class="cmd div_container">
			<div class="btn-group btn-group-justified" role="group">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="green_startrecord_btn" disabled>
						<span class="glyphicon glyphicon-film" aria-hidden="true"></span>
						<p>อัดเขียว</p>
					</button>
				</div>
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-orange" id="green_stoprecord_btn">
						<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
						<p>หยุดอัดเขียว</p>
					</button>
				</div>
			</div>
		</div>
		<br/>
		<div class="report div_container">
			<div class="btn-group btn-group-justified" role="group">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="chaiklang_camera_status_btn"><p class="status_text">สถานะกล้อง</p><p>ห้องกลาง</p></button>
				</div>
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="chaiklang_rec_status_btn"><p>ห้องกลางกำลังอัด</p><p class="duration">&nbsp;</p></button>
				</div>
			</div>
		</div>
		<div class="cmd div_container">
			<div class="btn-group btn-group-justified" role="group">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="chaiklang_startrecord_btn" disabled>
						<span class="glyphicon glyphicon-film" aria-hidden="true"></span>
						<p>อัดชายกลาง</p></button>
				</div>
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-orange" id="chaiklang_stoprecord_btn">
						<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
						<p>หยุดอัดชายกลาง</p></button>
				</div>
			</div>
		</div>
		<br/>
		<div class="report div_container">
			<div class="btn-group btn-group-justified" role="group">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="violet_camera_status_btn"><p class="status_text">สถานะกล้อง</p><p>ห้องม่วง</p></button>
				</div>
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="violet_rec_status_btn"><p>ห้องม่วงกำลังอัด</p><p class="duration">&nbsp;</p></button>
				</div>
			</div>
		</div>
		<div class="cmd div_container">
			<div class="btn-group btn-group-justified" role="group">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" id="violet_startrecord_btn" disabled>
						<span class="glyphicon glyphicon-film" aria-hidden="true"></span>
						<p>อัดม่วง</p></button>
				</div>
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-orange" id="violet_stoprecord_btn">
						<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
						<p>หยุดอัดม่วง</p></button>
				</div>
			</div>
		</div>
		<br/>
		<p><span class="glyphicon glyphicon glyphicon-menu-down show_live_down" aria-hidden="true"></span></p>
		<div id="live_control" style="display: none;">
			<div class="cmd div_container">
				<div class="btn-group btn-group-justified" role="group">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-info camera_reset_btn" id="green_room_camera_reset_btn">
							<span class="glyphicon glyphicon-camera" aria-hidden="true"></span>
							<p>รีเซ็ตสัญญาณภาพ</p> <p>ห้องเขียว</p>
						</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-info camera_reset_btn" id="chaiklang_room_camera_reset_btn">
							<span class="glyphicon glyphicon-camera" aria-hidden="true"></span>
							<p>รีเซ็ตสัญญาณภาพ</p> <p>ห้องชายกลาง</p>
						</button>
					</div>
				</div>
			</div>
			<br/>
			<div style="margin-bottom:5px; text-align:left; background:white; padding:10px;"  class="div_container" >
				<h4>ขั้นตอนการ Live FB เฉพาะห้องเขียว</h4>
				1. [บนเว็บ] เลือกว่าจะสอนบนกระดานหรือ Tablet <br/>
				2. [บนเว็บ] กดปุ่ม "เปิดโปรไฟล์ Live"<br/>
				3. ปรับซูมกล้องให้เรียบร้อย<br/>
				4. เปิดหน้า สร้างไลฟ์บน FB [<a href="https://www.facebook.com/live/create?step=landing" target="__blank">ลิงก์สร้างไลฟ์</a>]<br/>
				5. [FB] เลือกเพจที่จะไลฟ์ ใส่ข้อมูลต่างๆ<br/>
				6. [FB] ก๊อบ Stream Key มาไว้ใส่ในเว็บ<br/>
				<img src="vdo_control/asset/image/fblivekey.png" width="380px"><br/>
				7. [บนเว็บ] ใส่ Stream Key ในช่อง Facebook live Key<br/>
				8. [บนเว็บ] กด "เริ่ม Live"<br/>
				9. [FB] รอภาพมา กด Go Live (เริ่มแพร่ภาพสด)<br/>
			</div>
			<br/><br/>
			<div class="report div_container">
				<div class="btn-group btn-group-justified" role="group">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default" id="green_stream_status_btn"><span class="col-xs-6">กำลัง Live !</span><span class="col-xs-6" id="stream_time"></span></button>
					</div>
				</div>
			</div>
			<div style="margin-bottom:5px; text-align:left;" class="div_container form-control">
				สอนบน : 
				<label><input type="radio" name="fb_live_whiteboard_or_tablet" value="whiteboard" checked="checked"> Whiteboard&nbsp;&nbsp;&nbsp;</label>
				<label><input type="radio" name="fb_live_whiteboard_or_tablet" value="tablet"> Tablet</label>
			</div>
					<button type="button" class="btn btn-primary" id="load_fb_live_scene">
						<span class="glyphicon glyphicon-film" aria-hidden="true"></span>
						<p>เปิดโปรไฟล์ Live</p></button>
				</div>
			</div>
			<div style="margin-bottom:5px; " class="div_container">
				<input type="text" name="fb_live_server" id="fb_live_server" class="form-control" placeholder="Facebook Server" value="rtmps://live-api-s.facebook.com:443/rtmp/" style="margin-bottom: 3px;">
				<input type="text" name="fb_live_key" id="fb_live_key" class="form-control" placeholder="ใส่ Facebook Live key" value="2005322509792660?s_bl=0&s_ps=1&s_sw=0&s_vt=api-s&a=AbyeoPDEmaNTw4rs">
			</div>
				<div class="btn-group btn-group-justified" role="group">
							<p>เริ่ม Live</p></button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-orange" id="green_stopstream_btn">
							<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
							<p>หยุด Live</p></button>
					</div>
				</div>
			</div>

			<br/><br/><br/>
		</div>
	</center>
	<style type="text/css">
		body{
			width:100%;
			background-color: #5d5d5d;
		}
		.div_container{
			max-width: 400px;
		}
		.report{
			margin-bottom: 5px;
		}
		.btn{
			font-size: 16px;
			font-weight: bold;
		}
		.progress{
			margin-top: 5px;
			height: 16px;
		}
		.btn-orange{
			background-color: #F80;
			border-color: #7f603c;
			color: white;
		}
		.btn-wide{
			padding-left: 40px;
			padding-right: 40px;
		}
		select.form-control,
		input.form-control{
			margin-bottom: 5px;
		}
		.modal-dialog {
			margin: 80px auto;
		}

		.show_live_down{
			cursor: pointer;
		}
	</style>
</body>
</html>
