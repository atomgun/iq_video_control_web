<?php
define("DISK_SPACE_NORMAL", 	1);
define("DISK_SPACE_WARNING", 	2);
define("DISK_SPACE_CRITICAL", 	4);
///////////////////////////////////////////////////
//
// Get disk free space and total space 
// and low free space warning
// in JSON format
//
// Param : array of disk name
///////////////////////////////////////////////////
function get_disk_space_and_error_level($disk_name_array)
{
	$json_object = new stdClass();
	if($disk_name_array == null)
	{
		return json_encode($json_object);
	}

	$GB_class = 3;
	$si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
	$base = 1024;
	
	$warning_space = 30 * pow($base, $GB_class);
	$critical_space = 15 * pow($base, $GB_class);
	$tmp_level = 0;

	/////////////////////////
	// Space
	$json_object->disk_space = array();

	foreach ($disk_name_array as $disk_name) {	
		$df_bytes = disk_free_space($disk_name);
		$class = min((int)log($df_bytes , $base) , count($si_prefix) - 1);
		$free_space = sprintf('%1.2f' , $df_bytes / pow($base,$class)) . ' ' . $si_prefix[$class];
		
		$dt_bytes= disk_total_space($disk_name);
		$class = min((int)log($dt_bytes , $base) , count($si_prefix) - 1);
		$total_space = sprintf('%1.2f' , $dt_bytes / pow($base,$class)) . ' ' . $si_prefix[$class];

		$disk_space = array(
			"disk_name" => $disk_name,
			"free_space" => $free_space,
			"total_space" => $total_space
		);
		array_push($json_object->disk_space, $disk_space);

		if($df_bytes < $critical_space)
		{
			$tmp_level |= DISK_SPACE_CRITICAL;
		}
		else if($df_bytes < $warning_space)
		{
			$tmp_level |= DISK_SPACE_WARNING;
		}
		else
		{
			$tmp_level |= DISK_SPACE_NORMAL;
		}
	}


	/////////////////////////
	// Status

	$event_level = "";
	$event_msg = "";

	if($tmp_level >= DISK_SPACE_CRITICAL)
	{
		$event_level = "critical";
		$event_msg = "ERROR DISK SPACE !!!";
	}
	else if($tmp_level >= DISK_SPACE_WARNING)
	{
		$event_level = "warning";
		$event_msg = "WARNING DISK SPACE !!!";
	}
	else
	{
		$event_level = "normal";
		$event_msg = "";
	}
	$disk_status = array(
		"event_level" => $event_level,
		"event_msg" => $event_msg
	);

	$json_object->disk_status = $disk_status;

	return json_encode($json_object);
}

$disk_name = null;
if(isset($_GET["disk_name"]))
{
	$disk_name = $_GET["disk_name"];
}
else if(isset($_POST["disk_name"]))
{
	$disk_name = $_POST["disk_name"];
}
$sp_json = get_disk_space_and_error_level($disk_name);
echo $sp_json;
?>