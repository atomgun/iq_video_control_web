<?php 



/////////////////////////////////////
// Table name : iqplus_log
/////////////////////////////////////
/*
id int
ip_addr VARCHAR(32)
action LONGTEXT
details LONGTEXT
create_date timestamp

*/
function db_connect()
{
	$servername = "localhost";
	$db_user = "iqvdo";
	$db_pass = "iqplusvdo";
	$db_name = "iqplus_vdocontrol";

    $conn = new mysqli($servername, $db_user, $db_pass, $db_name);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
	} 
	$conn->query("SET CHARACTER SET utf8");
    return $conn;
}
function db_close($conn)
{
    $conn->close();
}
?>

