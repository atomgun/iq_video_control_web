<?php
include_once("db.php");

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
// Create a new student.
function add_anonymous_log( $action_string, $details='') {
    // add log.
    $dbcon = db_connect();
    $sql = "INSERT INTO iqplus_log (ip_addr, action, details) VALUES ('anonymous', {$action_string}, {$details});";
    if ($dbcon->query($sql) === TRUE) {
        echo "_anonymous Log add successfully";
    } else {
        echo "_anonymous Log add Error: " . $sql . "<br>" . $dbcon->error;
    }

    db_close($dbcon);
}
function add_log( $action_string, $details='') {
	$dbcon = db_connect();
	$ip_addr = mysqli_real_escape_string($dbcon, get_client_ip());
	$action_string = mysqli_real_escape_string($dbcon, $action_string);
	$details = mysqli_real_escape_string($dbcon, $details);
    $sql = "INSERT INTO iqplus_log (ip_addr, action, details) VALUES ('{$ip_addr}', '{$action_string}', '{$details}');";

    if ($dbcon->query($sql) === TRUE) {
        echo "Log add successfully";
    } else {
        echo "Log add Error: " . $sql . "<br>" . $dbcon->error;
    }

    db_close($dbcon);
}

// Get log of certain class.
function get_log($user_id) {
    // global $wpdb;
    // $school_log = 'iqplus_log';
    // $query_result = $wpdb->get_results("SELECT * FROM $school_log WHERE user_id = '$user_id' ", ARRAY_A);
    // return $query_result;
}

// Get all students.
function get_all_logs() {
    // global $wpdb;
    // $school_log = 'iqplus_log';
    // $query_result = $wpdb->get_results("SELECT * FROM $school_log ORDER BY log_id DESC", ARRAY_A);
    // return $query_result;
}


$action = isset($_POST["action"]) ? $_POST["action"] : isset($_GET["action"]) ? $_GET["action"] : "action not defined";
$details = isset($_POST["details"]) ? $_POST["details"] : isset($_GET["details"]) ? $_GET["details"] : "details not defined";
add_log( $action, $details);

// print_r($_SERVER);
//test url

//192.168.1.10/vdo_control/vdo_record_tool/log.php?action=test&details=asdfghjk
// INSERT INTO `iqplus_log`(`ip_addr`, `action`, `details`) VALUES (`::1`, `action not defined`, `details not defined`);
?>
