var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, timeout, options) {
			// Assigning defaults
			var dfd = jQuery.Deferred();
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			if(timeout != 'undefined')
			{
				setTimeout(function() {$dialog.modal('hide');dfd.resolve( "hidden" );}, timeout);
			}
			// Opening dialog
			$dialog.modal();
			return dfd.promise();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);

// OBS -->

var GREEN_ROOM = 99;
var CHAIKLANG_ROOM = 191;
var GREEN_ROOM_SCENE_COLLECTION_NAME 		= "Green_room";
var CHAIKLANG_ROOM_SCENE_COLLECTION_NAME 	= "Chaiklang_room";

var GREEN_ROOM_LIVE_SCENE_COLLECTION_NAME 		= "For_live_stream";

var WEBSOCKET_ROOM_NAME = {
	[GREEN_ROOM] 		: GREEN_ROOM_SCENE_COLLECTION_NAME + "    ",
	[CHAIKLANG_ROOM] 	: CHAIKLANG_ROOM_SCENE_COLLECTION_NAME
};
var hostname = document.location.hostname;
var wsocket_green_url = "ws://"+hostname+":4444/";
var wsocket_chaiklang_url = "ws://"+hostname+":4445/";
var websocket_green = null, websocket_chaiklang = null;
var statis_tid = 0;
var __is_green_recording = 0;
var __is_chaiklang_recording = 0;
var __is_green_streaming = 0;

var STATE_CONNECTING = 0;
var STATE_OPEN = 1;
var STATE_CLOSING = 2;
var STATE_CLOSED = 3;
var fern_subject = [ 
	{Key:"not_select", Value:"เลือกวิชา:---"},
	{Key:"Math_p5",Value:"Math ป.5" }, 
	{Key:"Math_p6",Value:"Math ป.6" }, 
	{Key:"Math_m1",Value:"Math ม.1" }, 
	{Key:"Math_m2",Value:"Math ม.2" }, 
	{Key:"Math_m3",Value:"Math ม.3" }, 
	{Key:"Math_m3_intensive",Value:"Math ม.3 พสวท" }, 
	{Key:"Math_m4",Value:"Math ม.4" }, 
	{Key:"Math_m5",Value:"Math ม.5" }, 
	{Key:"Math_m6",Value:"Math ม.6" }, 
	]; 
var kong_subject = [ 
	{Key:"not_select", Value:"เลือกวิชา:---"},
	{Key:"Sci_p5",Value:"Sci ป.5" }, 
	{Key:"Sci_p6",Value:"Sci ป.6" }, 
	{Key:"Sci_m1",Value:"Sci ม.1" }, 
	{Key:"Sci_m2",Value:"Sci ม.2" }, 
	{Key:"Sci_m3",Value:"Sci ม.3" }, 
	{Key:"Sci_m3_intensive",Value:"Sci ม.3 พสวท" }, 
	{Key:"Phy_m4",Value:"Phy ม.4" }, 
	{Key:"Phy_m5",Value:"Phy ม.5" }, 
	{Key:"Phy_m6",Value:"Phy ม.6" }, 
	]; 
var kankluay_subject = [
	{Key:"not_select", Value:"เลือกวิชา:---"},
	{Key:"Chem_m4",Value:"Chem ม.4" }, 
	{Key:"Chem_m5",Value:"Chem ม.5" },
	{Key:"Chem_m6",Value:"Chem ม.6" },
];
var bow_subject = [
	{Key:"not_select", Value:"เลือกวิชา:---"},
	{Key:"Eng_p6",Value:"Eng ป.6" }
];
// var FilenameFormat = "%CCYY-%MM-%DD_"+subject+"\\%CCYY-%b-%DD__%hh-%mm-%ss__"+subject;
// profile_name = math_m6_green
// profile_name = math_m5_chaiklang
// Events
	// Event Types
			// 	"SwitchScenes"
			// 	"ScenesChanged"
			// 	"SourceOrderChanged"
			// 	"SceneItemAdded"
			// 	"SceneItemRemoved"
			// 	"SceneItemVisibilityChanged"
			// 	"SceneCollectionChanged"
			// 	"SceneCollectionListChanged"
			// 	"SwitchTransition"
			// 	"TransitionDurationChanged"
			// 	"TransitionListChanged"
			// 	"TransitionBegin"
			// 	"ProfileChanged"
			// 	"ProfileListChanged"
			// 	"StreamStarting"
			// 	"StreamStarted"
			// 	"StreamStopping"
			// 	"StreamStopped"
			// 	"RecordingStarting"
			// 	"RecordingStarted"
			// 	"RecordingStopping"
			// 	"RecordingStopped"
			// 	"StreamStatus"
			// 	"Exiting"
// Requests
// Request Types
// REQUEST OK

// OBS-WEBSOCKET 4.2.0 
/*
available-requests : 
		Authenticate,
		DisableStudioMode,
		EnableStudioMode,
		GetAuthRequired,
		GetBrowserSourceProperties,
		GetCurrentProfile,
		GetCurrentScene,
		GetCurrentSceneCollection,
		GetCurrentTransition,
		GetMute,
		GetPreviewScene,
		GetRecordingFolder,
		GetSceneList,
		GetSpecialSources,
		GetStreamingStatus,
		GetStreamSettings,
		GetStudioModeStatus,
		GetSyncOffset,
		GetTextGDIPlusProperties,
		GetTransitionDuration,
		GetTransitionList,
		GetVersion,
		GetVolume,
		ListProfiles,
		ListSceneCollections,
		ResetSceneItem,
		SaveReplayBuffer,
		SaveStreamSettings,
		SetBrowserSourceProperties,
		SetCurrentProfile,
		SetCurrentScene,
		SetCurrentSceneCollection,
		SetCurrentTransition,
		SetMute,
		SetPreviewScene,
		SetRecordingFolder,
		SetSceneItemCrop,
		SetSceneItemPosition,
		SetSceneItemRender,
		SetSceneItemTransform,
		SetSourceRender,
		SetStreamSettings,
		SetSyncOffset,
		SetTextGDIPlusProperties,
		SetTransitionDuration,
		SetVolume,
		StartRecording,
		StartReplayBuffer,
		StartStopRecording,
		StartStopReplayBuffer,
		StartStopStreaming,
		StartStreaming,
		StopRecording,
		StopReplayBuffer,
		StopStreaming,
		ToggleMute,
		ToggleStudioMode,
		TransitionToProgram
*/
	var get_version 			= {"request-type": "GetVersion" , 			"message-id" : "getversion"};
	var get_auth_require 		= {"request-type": "GetAuthRequired" , 		"message-id" : "1"};
	var set_authen	 			= {"request-type": "Authenticate" , 		"message-id" : "1"};
	var get_scene_list 			= {"request-type": "GetSceneList" , 		"message-id" : "getscenelist"};
	var get_current_scene 		= {"request-type": "GetCurrentScene" , 		"message-id" : "getcurrentscene"};
	var set_current_scene 		= {"request-type": "SetCurrentScene" , 		"message-id" : "setcurrentscene", "scene-name" : ""};
	var set_source_render		= {"request-type": "SetSourceRender" , 		"message-id" : "setsourcerender", "source": "", "render":"true", "scene-name": ""};
	// "render" (bool) : desired visibility
	// "scene-name" (string; optional) : name of the scene the source belongs to. defaults to current scene.

	
	
	var start_stream			= {"request-type": "StartStreaming" , 		"message-id" : "startstream"};
	var stop_stream				= {"request-type": "StopStreaming" , 		"message-id" : "stopstream"};
	var set_start_stop_stream	= {"request-type": "StartStopStreaming" , 	"message-id" : "streamtoggle"};
	
	
	var start_record			= {"request-type": "StartRecording" , 		"message-id" : "startrecord"};
	var stop_record				= {"request-type": "StopRecording" , 		"message-id" : "stoprecord"};
	var set_start_stop_record	= {"request-type": "StartStopRecording" , 	"message-id" : "recordtoggle"};
	var get_stream_status		= {"request-type": "GetStreamingStatus" , 	"message-id" : "recordstatus"};

	var get_stream_setting		= {"request-type": "GetStreamSettings" , 	"message-id" : "getstreamsetting"};
	var set_stream_setting		= {"request-type": "SetStreamSettings" , 	"message-id" : "setstreamsetting"};
	
	var get_transition_list		= {"request-type": "GetTransitionList" , 	"message-id" : "1"};
	var get_current_transition	= {"request-type": "GetCurrentTransition" , "message-id" : "1"};
	var set_current_transition 	= {"request-type": "SetCurrentTransition" , "message-id" : "set_current_transition"};
	var get_transition_duration = {"request-type": "SetTransitionDuration", "message-id" : "1"};
	var set_volumn 				= {"request-type": "SetVolume" , 			"message-id" : "1"};
	var get_volumn 				= {"request-type": "GetVolume" , 			"message-id" : "1", "source" : ""};
	var set_mute 				= {"request-type": "SetMute" , 				"message-id" : "1"};
	var set_toggle_mute 		= {"request-type": "ToggleMute" , 			"message-id" : "1"};
	// var get_version 			= {"request-type": "SetSceneItemPosition", 	"message-id" : "1"};
	// var get_version 			= {"request-type": "SetSceneItemTransform",	"message-id" : "1"};
	var set_current_scene_collection = {"request-type": "SetCurrentSceneCollection", 	"message-id" : "set_current_scene_collection", "sc-name":""};
	var get_current_scene_collection = {"request-type": "GetCurrentSceneCollection", 	"message-id" : "get_current_scene_collection"};
	var list_scene_collection 	= {"request-type": "ListSceneCollections", 	"message-id" : "list_scene_collection"};
	var set_current_profile 	= {"request-type": "SetCurrentProfile", 	"message-id" : "set_current_profile", "profile-name":""};
	var get_current_profile 	= {"request-type": "GetCurrentProfile", 	"message-id" : "get_current_profile"};
	var list_profile 			= {"request-type": "ListProfiles", 			"message-id" : "listprofile"};
// Authentication






function reset_teacher_and_subject()
{
	Key = "not_select";
	Value = "เลือกวิชา:---"; 
	option = new Option(Value, Key); 
	$("select#subject").empty().append($(option));
	$("select#teacher").val("not_select").change();
}

function get_websocket_room(websocket)
{
	if(websocket.url == wsocket_green_url)
	{
		return GREEN_ROOM;
	}
	else if(websocket.url == wsocket_chaiklang_url)
	{
		return CHAIKLANG_ROOM;
	}
	else
	{
		return 0;
	}
}
function handleWebsocketUpdate(websocket, json_update)
{
	if(get_websocket_room(websocket) == GREEN_ROOM)
	{
		switch(json_update["update-type"])
		{
			case "StreamStatus":
				if(json_update.streaming == true)
				{
					time = json_update["stream-timecode"].split('.')[0];
					$("#green_stream_status_btn #stream_time").text(time);
					$("#green_stream_status_btn").removeClass('btn-default').addClass('btn-danger');
				}
				else if(json_update.streaming == false)
				{
					$("#green_stream_status_btn #stream_time").text("");
					$("#green_stream_status_btn").removeClass('btn-danger').addClass('btn-default');
				}

				break;
			case "RecordingStopped":
				$("#grn_rec_status_btn").removeClass('btn-danger').addClass('btn-default');
				$("#grn_rec_status_btn p.duration").text("-");
				__is_green_recording = false;
				// refresh_status(websocket);
				break;
			case "RecordingStarted":
				$("#grn_rec_status_btn").removeClass('btn-default').addClass('btn-danger');
				reset_teacher_and_subject();
				refresh_status(websocket);
				__is_green_recording = true;
				break;
			case "SceneItemVisibilityChanged":
				if(json_update["item-name"] == "Yeah_iam_finish")
				{
					// item-visible = true
				}
				else if(json_update["item-name"] == "IQPLUS_Slide_end")
				{

				}

				break;
			case "TransitionBegin":

				break;

			case "SwitchScenes":

				break;
			case "ProfileChanged":
				reset_teacher_and_subject();
				break;
			default:
				break;

		}
	}
	else if(get_websocket_room(websocket) == CHAIKLANG_ROOM)
	{
		switch(json_update["update-type"])
		{
			case "RecordingStopped":
				$("#chaiklang_rec_status_btn").removeClass('btn-danger').addClass('btn-default');
				$("#chaiklang_rec_status_btn p.duration").text("-");
				__is_chaiklang_recording = false;
				// refresh_status(websocket);
				break;
			case "RecordingStarted":
				$("#chaiklang_rec_status_btn").removeClass('btn-default').addClass('btn-danger');
				__is_chaiklang_recording = true;
				reset_teacher_and_subject();
				refresh_status(websocket);
				break;
			case "ProfileChanged":
				reset_teacher_and_subject();
				break;

			default:
				break;

		}
	}
}
function handleWebsocketResponse(websocket, json_resp)
{
	if(get_websocket_room(websocket) == GREEN_ROOM)
	{

		switch(json_resp["message-id"])
		{
			case "recordstatus":
				if(json_resp.recording == true)
				{
					$("#grn_rec_status_btn").removeClass('btn-default').addClass('btn-danger');
					__is_green_recording = true;
				}
				else if(json_resp.recording == false)
				{
					$("#grn_rec_status_btn").removeClass('btn-danger').addClass('btn-default');
					$("#grn_rec_status_btn p.duration").text("-");
					__is_green_recording = false;
				}

				if(json_resp.streaming == true)
				{
					$("#green_stream_status_btn").removeClass('btn-default').addClass('btn-danger');
					__is_green_streaming = true;
				}
				else if(json_resp.streaming == false)
				{
					$("#green_stream_status_btn").removeClass('btn-danger').addClass('btn-default');
					$("#green_stream_status_btn #stream_time").text("");
					__is_green_streaming = false;
				}

				break;
			case "recordtoggle":

				break;
			case "streamtoggle":
				break;
			case "getscenelist":

				break;
			case "setsourcerender":
				if(json_resp["status"] === "ok")
				{
					datasend = set_current_scene;
					datasend["scene-name"] = "Green_room";
					doSend(websocket, JSON.stringify(datasend));
					// console.log(datasend["request-type"]);
					// console.log("Transition scene");
				}
				break;
			case "open_Green_room_collection":
				if(json_resp["status"] === "ok")
				{
					$("#grn_camera_status_btn").removeClass('btn-default').addClass('btn-success');
				}
				else
				{
					$("#grn_camera_status_btn").removeClass('btn-success').addClass('btn-default');
				}
				break;
			case "get_current_scene_collection":
				if(json_resp["sc-name"] === GREEN_ROOM_SCENE_COLLECTION_NAME)
				{
					$("#grn_camera_status_btn").removeClass('btn-default').addClass('btn-success');
				}
				else
				{
					$("#grn_camera_status_btn").removeClass('btn-success').addClass('btn-default');
				}
				break;
			case "get_current_profile":
				if(__is_green_recording)
				{
					profile = json_resp["profile-name"].split("_");
					$("#grn_rec_status_btn p.duration").text(profile[0] + " " + profile[1]);
				}
				else
				{
					$("#grn_rec_status_btn p.duration").text("-");
				}
				break;
			default:
				break;

		}
	}
	else if(get_websocket_room(websocket) == CHAIKLANG_ROOM)
	{
		switch(json_resp["message-id"])
		{
			case "recordstatus":
				if(json_resp.recording == true)
				{
					$("#chaiklang_rec_status_btn").removeClass('btn-default').addClass('btn-danger');
					__is_chaiklang_recording = true;
				}
				else
				{
					$("#chaiklang_rec_status_btn").removeClass('btn-danger').addClass('btn-default');
					$("#chaiklang_rec_status_btn p.duration").text("-");
					__is_chaiklang_recording = false;
				}
				break;
			case "recordtoggle":

				break;
			case "streamtoggle":
			
				break;
			case "getscenelist":

				break;
			case "setsourcerender":
				if(json_resp["status"] === "ok")
				{
					datasend = set_current_scene;
					datasend["scene-name"] = "Chaiklang_room";
					doSend(websocket, JSON.stringify(datasend));
					// console.log(datasend["request-type"]);
					// console.log("Transition scene");
				}
				break;
			case "open_Chaiklang_room_collection":
				if(json_resp["status"] === "ok")
				{
					$("#chaiklang_camera_status_btn").removeClass('btn-default').addClass('btn-success');
				}
				else
				{
					$("#chaiklang_camera_status_btn").removeClass('btn-success').addClass('btn-default');
				}
				break;
			case "get_current_scene_collection":
				if(json_resp["sc-name"] === CHAIKLANG_ROOM_SCENE_COLLECTION_NAME)
				{
					$("#chaiklang_camera_status_btn").removeClass('btn-default').addClass('btn-success');
				}
				else
				{
					$("#chaiklang_camera_status_btn").removeClass('btn-success').addClass('btn-default');
				}
				break;
			case "get_current_profile":
				if(__is_chaiklang_recording)
				{
					profile = json_resp["profile-name"].split("_");
					$("#chaiklang_rec_status_btn p.duration").text(profile[0] + " " + profile[1]);
				}
				else
				{
					$("#chaiklang_rec_status_btn p.duration").text("-");
				}
				break;
			default:
				break;

		}
	}
	

	return;
}
function refresh_status(websocket)
{
	if(get_websocket_room(websocket) == GREEN_ROOM)
	{
		doSend(websocket, JSON.stringify(get_stream_setting));
	}
	doSend(websocket, JSON.stringify(get_stream_status));
	doSend(websocket, JSON.stringify(get_current_scene_collection));
	doSend(websocket, JSON.stringify(get_current_profile));
}
function status_monitor(websocket)
{
	refresh_status(websocket);
	// statis_tid = 
	setTimeout(status_monitor, 5000, websocket); // repeat myself
}
function intiWebSocket()
{
	if(websocket_green == null || websocket_green.readyState === STATE_CLOSED)
	{
		websocket_green = new WebSocket(wsocket_green_url);
		websocket_green.onopen = function(evt) { onOpen(websocket_green, evt) };
		websocket_green.onclose = function(evt) { onClose(websocket_green, evt) };
		websocket_green.onmessage = function(evt) { onMessage(websocket_green, evt) };
		websocket_green.onerror = function(evt) { onError(websocket_green, evt) };
	}
	if(websocket_chaiklang == null || websocket_chaiklang.readyState === STATE_CLOSED)
	{
		websocket_chaiklang = new WebSocket(wsocket_chaiklang_url);
		websocket_chaiklang.onopen = function(evt) { onOpen(websocket_chaiklang, evt) };
		websocket_chaiklang.onclose = function(evt) { onClose(websocket_chaiklang, evt) };
		websocket_chaiklang.onmessage = function(evt) { onMessage(websocket_chaiklang, evt) };
		websocket_chaiklang.onerror = function(evt) { onError(websocket_chaiklang, evt) };
	}
}

function onOpen(websocket, evt)
{
	name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	// console.log("("+name+") CONNECTED");
	status_monitor(websocket);
	// doSend(websocket, JSON.stringify(get_version));
}
function onClose(websocket, evt)
{
	name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	// console.log("("+name+") DISCONNECTED");
	setTimeout(intiWebSocket, 5000);
}
function onMessage(websocket, evt)
{
	console.log(evt);
	var resp = jQuery.parseJSON(event.data);
	name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	if(resp["message-id"] == null)
	{		
		console.log("UPDATE   ("+name+") : " + evt.data.replace(/(\r\n|\n|\r)/g,""));
		handleWebsocketUpdate(websocket, resp);
	}
	else
	{
		console.log("RESPONSE ("+name+") : " + evt.data.replace(/(\r\n|\n|\r)/g,""));
		handleWebsocketResponse(websocket, resp);
	}
}
function onError(websocket, evt)
{
	name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	console.log( "Error ("+name+") : " + evt.data);
	setTimeout(intiWebSocket, 5000);
}
function doSend(websocket, message)
{
	name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	if(websocket.readyState != STATE_OPEN)
	{
		console.log("WEBSOCKET ("+name+") NOT OPEN");
		intiWebSocket();
	}
	// console.log("SENT     ("+name+") : " + message);
	websocket.send(message);
}
function Show_yeah_iam_finish_and_transition(websocket)
{
	datasend = set_source_render;
	datasend.source = "Yeah_iam_finish";
	datasend.render = true;
	// datasend["scene-name"] = "Default";
	doSend(websocket, JSON.stringify(datasend));
	// console.log(datasend["request-type"]);
	// console.log("Show i am finish");

	// it will transition when recv RESP
}
function Hide_yeah_iam_finish_and_transition(websocket)
{
	datasend = set_source_render;
	datasend.source = "Yeah_iam_finish";
	datasend.render = false;
	// datasend["scene-name"] = "Default";
	doSend(websocket, JSON.stringify(datasend));
	// console.log(datasend["request-type"]);
	// console.log("Hide i am finish");

	// it will transition when recv RESP
}
function Show_IQPLUS_Slide_and_transition(websocket)
{
	datasend = set_source_render;
	datasend.source = "IQPLUS_Slide_end";
	datasend.render = true;
	// datasend["scene-name"] = "Default";
	doSend(websocket, JSON.stringify(datasend));
	// console.log(datasend["request-type"]);
	// console.log("Show IQPLUS SLIDE");
}
function Hide_IQPLUS_Slide_and_transition(websocket)
{
	datasend = set_source_render;
	datasend.source = "IQPLUS_Slide_end";
	datasend.render = false;
	// datasend["scene-name"] = "Default";
	doSend(websocket, JSON.stringify(datasend));
	// console.log(datasend["request-type"]);
	// console.log("Hide IQPLUS SLIDE");
}
var blink_on = false;
var blink_tid = 0;
var last_level = "";
function blink_red_background()
{
	if(blink_on){
		$("body").css("background-color", "#5d5d5d");
		blink_on = false;
	}
	else{
		$("body").css("background-color", "#FF3639");
		blink_on = true;
	}
}
function blink_yellow_background()
{
	//FFE85C
	if(blink_on){
		$("body").css("background-color", "#5d5d5d");
		blink_on = false;
	}
	else{
		$("body").css("background-color", "#FFE85C");
		blink_on = true;
	}
}
function blink_clear_background()
{
	$("body").css("background-color", "#5d5d5d");
	blink_on = false;
}
function check_disk_space()
{
	param = { "disk_name" : ["C:", "D:"]};
	get = $.getJSON("/vdo_control/vdo_record_tool/check_disk_space.php", param, function(data, textStatus, jqXHR){
		disk_status = data.disk_status;
		if(disk_status.event_level == "critical")
		{
			// set red blink
			if(blink_tid != 0 && last_level != disk_status.event_level){
				// change level
				//clear last tid
				clearInterval(blink_tid);
				blink_tid = setInterval(blink_red_background, 500);
			}
			else if(blink_tid == 0)
			{
				// last_level is normal
				blink_tid = setInterval(blink_red_background, 500);
			}
		}
		else if(disk_status.event_level == "warning")
		{
			// console.log("SET YELLOW");
			// set yellow
			if(blink_tid != 0 && last_level != disk_status.event_level){
				// change level
				//clear last tid
				clearInterval(blink_tid);
				blink_tid = setInterval(blink_yellow_background, 1000);
			}
			else if(blink_tid == 0)
			{
				// last_level is normal
				blink_tid = setInterval(blink_yellow_background, 1000);
			}
		}
		else if(disk_status.event_level == "normal")
		{
			// clear color
			clearInterval(blink_tid);
			blink_clear_background();
		}
		last_level = disk_status.event_level;
	});
	get.fail(function(jqXHR, textStatus, errorThrown) {
		// $( "#result" ).html( "error" );
		console.log(jqXHR.responseText);
		console.log(textStatus);
	});
}
function init_check_disk_space()
{
	check_disk_space();
	var disk_space_tid = setInterval(check_disk_space, 60 * 1000);
}
jQuery( document ).ready(function($) {
	intiWebSocket();
	init_check_disk_space();
	$('#green_loadpreset_btn').on('click', function() {
		datasend = list_scene_collection;
		doSend(websocket_green, JSON.stringify(datasend));
		// console.log(datasend["request-type"]);

		// Green_room
		set_current_scene_collection["message-id"] = "open_Green_room_collection";
		set_current_scene_collection["sc-name"] = "Green_room";
		datasend = set_current_scene_collection;
		doSend(websocket_green, JSON.stringify(datasend));
		// console.log("atomgun"+datasend["request-type"] + " " + datasend["sc-name"]);
	});
	$('#green_startrecord_btn').on('click', function() {
		if(!__is_green_recording)
		{
			if($("select#subject").val() == "not_select")
			{
				// alert("เลือกวิชาก่อนจ้า");
				waitingDialog.show("เลือกวิชาก่อนจ้า", 2000);
				return;
			}
			else
			{
				// change profile
				profile_name = $("select#subject").val()+"_green_profile";
				datasend = set_current_profile;
				datasend["profile-name"] = profile_name;
				doSend(websocket_green, JSON.stringify(datasend));
				// console.log(datasend["request-type"]);
				// console.log("Chang profile to : " + profile_name);
			}

			// console.log("open Green_Unseen preset");
			set_current_scene_collection["message-id"] = "open_Green_room_Unseen_collection";
			set_current_scene_collection["sc-name"] = "Unseen";
			datasend = set_current_scene_collection;
			doSend(websocket_green, JSON.stringify(datasend));
			// console.log(datasend["request-type"] + " " + datasend["sc-name"]);


			// timeout for open Green scene collection  500 ms
			setTimeout(function(){
				console.log("open green preset");		
				// Green room
				set_current_scene_collection["message-id"] = "open_Green_room_collection";
				set_current_scene_collection["sc-name"] = "Green_room";
				datasend = set_current_scene_collection;
				doSend(websocket_green, JSON.stringify(datasend));
				// console.log(datasend["request-type"] + " " + datasend["sc-name"]);


				// transition effect before start
				Hide_yeah_iam_finish_and_transition(websocket_green);
				Hide_IQPLUS_Slide_and_transition(websocket_green);
				// start record after hide 500ms
				setTimeout(function(){
					datasend = set_start_stop_record;
					doSend(websocket_green, JSON.stringify(datasend));
					// console.log(datasend["request-type"]);
					// console.log("Start record green");

					reset_teacher_and_subject();
					refresh_status(websocket_green);
					// datasend = set_start_stop_stream;
					// doSend(websocket_green, JSON.stringify(datasend));
					// console.log(datasend["request-type"]);
					// console.log("Start stream green");
				}, 800);

			}, 500);

			
		}		
	});
	$('#green_stoprecord_btn').on('click', function() {
		if(__is_green_recording)
		{
			bootbox.confirm({
				message: '<h3 style="margin:0;">หยุดอัดห้องเขียว ?<!--<br>กด yes แล้วรอ 10 วิค่อยปิดมือถือ--></h3>',
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success btn-wide'
					},
					cancel: {
						label: ' No ',
						className: 'btn-danger btn-wide'
					}
				},
				callback: function (result) {
					if(result == true)
					{
						// transition effect before stop
						// Show_yeah_iam_finish_and_transition(websocket_green);
						// setTimeout(Show_IQPLUS_Slide_and_transition, 5000, websocket_green);

						// stop after 10 sec of 2 transition
						// setTimeout(function(){
							datasend = set_start_stop_record;
							doSend(websocket_green, JSON.stringify(datasend));
							// console.log(datasend["request-type"]);
							// console.log("Stop record green");

							reset_teacher_and_subject();
							refresh_status(websocket_green);
							// datasend = set_start_stop_stream;
							// doSend(websocket_green, JSON.stringify(datasend));
							// console.log(datasend["request-type"]);
							// console.log("Stop stream green");


						// }, 10000);
					}
				}
			});
		}
	});

	//////////////////////////
	// CHAIKLANG  CMD
	$('#chaiklang_loadpreset_btn').on('click', function() {
		// oen Unseen preset for reset scene collection
		// console.log("open chaiklang_Unseen preset");
		set_current_scene_collection["message-id"] = "open_Chaiklang_room_Unseen_collection";
		set_current_scene_collection["sc-name"] = "Unseen";
		datasend = set_current_scene_collection;
		doSend(websocket_chaiklang, JSON.stringify(datasend));
		// console.log(datasend["request-type"] + " " + datasend["sc-name"]);


		setTimeout(function(){
			// console.log("open chaiklang preset");		
			// Chaiklang_room
			set_current_scene_collection["message-id"] = "open_Chaiklang_room_collection";
			set_current_scene_collection["sc-name"] = "Chaiklang_room";
			datasend = set_current_scene_collection;
			doSend(websocket_chaiklang, JSON.stringify(datasend));
			// console.log(datasend["request-type"] + " " + datasend["sc-name"]);
		}, 500);
		
	});
	$('#chaiklang_startrecord_btn').on('click', function() {
		if(!__is_chaiklang_recording)
		{
			if($("select#subject").val() == "not_select")
			{
				// alert("เลือกวิชาก่อนจ้า");
				waitingDialog.show("เลือกวิชาก่อนจ้า", 2000);
				return;
			}
			else
			{
				// change profile
				profile_name = $("select#subject").val()+"_chaiklang_profile";
				datasend = set_current_profile;
				datasend["profile-name"] = profile_name;
				doSend(websocket_chaiklang, JSON.stringify(datasend));
				// console.log(datasend["request-type"]);
				// console.log("Chang profile to : " + profile_name);
			}

			// console.log("open chaiklang_Unseen preset");
			set_current_scene_collection["message-id"] = "open_Chaiklang_room_Unseen_collection";
			set_current_scene_collection["sc-name"] = "Unseen";
			datasend = set_current_scene_collection;
			doSend(websocket_chaiklang, JSON.stringify(datasend));
			// console.log(datasend["request-type"] + " " + datasend["sc-name"]);

			// timeout for open chaicklan scene collection  500 ms
			setTimeout(function(){
				// console.log("open chaiklang preset");		
				// Chaiklang_room
				set_current_scene_collection["message-id"] = "open_Chaiklang_room_collection";
				set_current_scene_collection["sc-name"] = "Chaiklang_room";
				datasend = set_current_scene_collection;
				doSend(websocket_chaiklang, JSON.stringify(datasend));
				// console.log(datasend["request-type"] + " " + datasend["sc-name"]);

				// transition effect before start
				Hide_yeah_iam_finish_and_transition(websocket_chaiklang);
				Hide_IQPLUS_Slide_and_transition(websocket_chaiklang);
				// start record after hide 800ms
				setTimeout(function(){
					datasend = set_start_stop_record;
					doSend(websocket_chaiklang, JSON.stringify(datasend));
					// console.log(datasend["request-type"]);
					// console.log("Start record chaiklang");

					reset_teacher_and_subject();
					refresh_status(websocket_chaiklang);
				}, 800);

			}, 500);
		}	
	});
	$('#chaiklang_stoprecord_btn').on('click', function() {
		if(__is_chaiklang_recording)
		{
			bootbox.confirm({
				message: '<h3 style="margin:0;">หยุดอัดห้องชายกลาง ?<!--<br>กด yes แล้วรอ 10 วิค่อยปิดมือถือ--></h3>',
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success  btn-wide'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger  btn-wide'
					}
				},
				callback: function (result) {
					if(result == true)
					{
						// transition effect before stop
						// Show_yeah_iam_finish_and_transition(websocket_chaiklang);
						// setTimeout(Show_IQPLUS_Slide_and_transition, 5000, websocket_chaiklang);

						// stop after 10 sec of 2 transition
						// setTimeout(function(){
							datasend = set_start_stop_record;
							doSend(websocket_chaiklang, JSON.stringify(datasend));
							// console.log(datasend["request-type"]);
							// console.log("Stop record chaiklang");

							reset_teacher_and_subject();
							refresh_status(websocket_chaiklang);
						// }, 10000);
					}
				}
			});
			
			
		}
	});
	$("#green_startstream_btn").click(function(){	
		if(!__is_green_streaming)
		{
			fb_live_key = $("#fb_live_key").val();
			if(fb_live_key == null || fb_live_key == "")
			{
				var ret = 0;
				$.when(waitingDialog.show("ใส่ Facebook Live key จ้า", 1300))
				.then(function(){
					$("#fb_live_key").focus();

					return -1;
				});
			}
			else
			{
				exe_time = 0;
				// change profile
				profile_name = "Math_m6_green_profile";
				datasend = set_current_profile;
				datasend["profile-name"] = profile_name;
				doSend(websocket_green, JSON.stringify(datasend));
				// console.log(datasend["request-type"]);
				// console.log("Chang profile to : " + profile_name);			
				exe_time += 200;
				setTimeout(function(){
					var dfd1 = jQuery.Deferred();
					// console.log("open Green_Unseen preset");
					set_current_scene_collection["message-id"] = "open_Green_room_Unseen_collection";
					set_current_scene_collection["sc-name"] = "Unseen";
					datasend = set_current_scene_collection;
					doSend(websocket_green, JSON.stringify(datasend));
					// console.log(datasend["request-type"] + " " + datasend["sc-name"]);
				}, exe_time);

				exe_time += 500;
				setTimeout(function(){
					// console.log("open Live green preset");
					// Green room
					set_current_scene_collection["message-id"] = "open_Live_collection";
					set_current_scene_collection["sc-name"] = GREEN_ROOM_LIVE_SCENE_COLLECTION_NAME;
					datasend = set_current_scene_collection;
					doSend(websocket_green, JSON.stringify(datasend));
					// console.log(datasend["request-type"] + " " + datasend["sc-name"]);
				}, exe_time);

				exe_time += 300;
				setTimeout(function(){
					// start stream
					datasend = set_stream_setting;
					// datasend.type = "rtmp_common";
					datasend.type = "rtmp_custom";
					datasend.settings = Object();
					datasend.settings.server = "rtmp://live-api-a.facebook.com:80/rtmp/"
					// datasend.settings.service = "Facebook Live";
					datasend.settings.key = fb_live_key;
					// datasend.settings.key = "10155215525679234?ds=1&s_l=1&a=ATiMwdI8zLp-BDN2";
					datasend.settings["use-auth"] = false;
					datasend.save = true;
					doSend(websocket_green, JSON.stringify(datasend));
				}, exe_time);

				exe_time += 500;
				setTimeout(function(){
					var dfd4 = jQuery.Deferred();
					doSend(websocket_green, JSON.stringify(start_stream));
					// console.log(datasend["request-type"]);
					// console.log("Start stream green");
					refresh_status(websocket_green);
				}, exe_time);

			}
		}
	});
	$("#green_stopstream_btn").click(function(){
		if(__is_green_streaming)
		{
			bootbox.confirm({
				message: '<h3 style="margin:0;">หยุด Live ?</h3>',
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success  btn-wide'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger  btn-wide'
					}
				},
				callback: function (result) {
					if(result == true)
					{
						datasend = stop_stream;
						doSend(websocket_green, JSON.stringify(datasend));
						// console.log(datasend["request-type"]);
						// console.log("Stop stream green");
						refresh_status(websocket_green);
						// clear key input 
						$("#fb_live_key").val("");
					}
				}
			});

		}
	});
	$("select#teacher").on('change', function() {
		subject_array = null;
		if(this.value == "fern")
		{
			subject_array = fern_subject;
		}
		else if(this.value == "kong")
		{
			subject_array = kong_subject;
		}
		else if(this.value == "kankluay")
		{
			subject_array = kankluay_subject;
		}
		else if(this.value == "bow")
		{
			subject_array = bow_subject;
		}
		if(subject_array != null)
		{
			$("select#subject").empty();
			$.each(subject_array, function() { 
				// this.Key and this.Value refer to array(key, value) of subject
				var option = new Option(this.Value, this.Key); 
				$("select#subject").append($(option));
			}); 
		}
	});

	$(".show_live_down").click(function(){
		$("#live_control").toggle();
	});
});

// END OBS 
