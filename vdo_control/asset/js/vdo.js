// vdo.js


// $.loadScript = function (url, callback) {
// 	jQuery.ajax({
// 		url: url,
// 		dataType: 'script',
// 		success: callback,
// 		async: true
// 	});
// }


// $.loadScript('url_to_someScript.js', function(){
//     //Stuff to do after someScript has loaded
// });
function add_log(action, details)
{
	data = {};
	data["action"] = action;
	data["details"] = details;
	$.ajax({
		url: "/vdo_control/vdo_record_tool/log.php",
		data: data,
		// success: success,
		// dataType: dataType
	});
}
var iq_debug = false;
var waitingDialog = waitingDialog || (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, timeout, options) {
			// Assigning defaults
			var dfd = jQuery.Deferred();
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			if(timeout != 'undefined')
			{
				setTimeout(function() {$dialog.modal('hide');dfd.resolve( "hidden" );}, timeout);
			}
			// Opening dialog
			$dialog.modal();
			return dfd.promise();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);


var STATE_CONNECTING = 0;
var STATE_OPEN = 1;
var STATE_CLOSING = 2;
var STATE_CLOSED = 3;
///
var GREEN_ROOM = 0;
var CHAIKLANG_ROOM = 1;
var VIOLET_ROOM = 2;
var GREEN_ROOM_EXTEND = 3;
///
var GREEN_ROOM_NAME 		= "Green_room";
var GREEN_ROOM_EXTEND_NAME 	= "Green_room_extend";
var CHAIKLANG_ROOM_NAME 	= "Chaiklang_room";
var VIOLET_ROOM_NAME 		= "Violet_room";
///
var GREEN_ROOM_SCENE_COLLECTION_NAME 		= "Green_room";
var GREEN_ROOM_EXTEND_SCENE_COLLECTION_NAME = "Green_room_Extend";
var CHAIKLANG_ROOM_SCENE_COLLECTION_NAME 	= "Chaiklang_room";
var VIOLET_ROOM_SCENE_COLLECTION_NAME 		= "Violet_room";
///
var GREEN_ROOM_FB_LIVE_WHITEBOARD_SCENE_COLLECTION_NAME = "Facebook_live_Whiteboard";
var GREEN_ROOM_FB_LIVE_TABLET_SCENE_COLLECTION_NAME = "Facebook_live_Tablet";

///
var WEBSOCKET_ROOM_NAME = {
	[GREEN_ROOM] 		: GREEN_ROOM_SCENE_COLLECTION_NAME + "    ",
	[CHAIKLANG_ROOM] 	: CHAIKLANG_ROOM_SCENE_COLLECTION_NAME,
	[VIOLET_ROOM] 		: VIOLET_ROOM_SCENE_COLLECTION_NAME,
	[GREEN_ROOM_EXTEND] : GREEN_ROOM_EXTEND_SCENE_COLLECTION_NAME,
};

var hostname = document.location.hostname;
// var hostname = "192.168.1.10";
var wsocket_green_url = "ws://"+hostname+":4444/";
var wsocket_chaiklang_url = "ws://"+hostname+":4445/";
var wsocket_violet_url = "ws://"+hostname+":4446/";
var wsocket_green_extend_url = "ws://"+hostname+":4447/";

var websocket_green_obj = null;
var websocket_chaiklang_obj = null;
var websocket_violet_obj = null;
var websocket_green_extend_obj = null;

var get_version 			= {"is_send": 0, "request-type": "GetVersion" , 			"message-id" : "GetVersion"};
var get_auth_require 		= {"is_send": 0, "request-type": "GetAuthRequired" , 		"message-id" : "GetAuthRequired"};
var set_authen	 			= {"is_send": 0, "request-type": "Authenticate" , 		"message-id" : "Authenticate"};
var get_scene_list 			= {"is_send": 0, "request-type": "GetSceneList" , 		"message-id" : "GetSceneList"};
var get_current_scene 		= {"is_send": 0, "request-type": "GetCurrentScene" , 		"message-id" : "GetCurrentScene"};
var set_current_scene 		= {"is_send": 0, "request-type": "SetCurrentScene" , 		"message-id" : "SetCurrentScene", "scene-name" : ""};
var set_source_render		= {"is_send": 0, "request-type": "SetSourceRender" , 		"message-id" : "SetSourceRender", "source": "", "render":"true", "scene-name": ""};





var start_record			= {"is_send": 0, "request-type": "StartRecording" , 		"message-id" : "StartRecording"};
var stop_record				= {"is_send": 0, "request-type": "StopRecording" , 		"message-id" : "StopRecording"};
var set_start_stop_record	= {"is_send": 0, "request-type": "StartStopRecording" , 	"message-id" : "StartStopRecording"};
var get_stream_status		= {"is_send": 0, "request-type": "GetStreamingStatus" , 	"message-id" : "GetStreamingStatus"};


var start_stream 			= { "is_send": 0, "request-type": "StartStreaming", "message-id": "StartStreaming" };
var stop_stream 			= { "is_send": 0, "request-type": "StopStreaming", "message-id": "StopStreaming" };
var set_start_stop_stream 	= { "is_send": 0, "request-type": "StartStopStreaming", "message-id": "StartStopStreaming" };
var get_stream_setting		= {"is_send": 0, "request-type": "GetStreamSettings" , 	"message-id" : "GetStreamSettings"};
var set_stream_setting		= {"is_send": 0, "request-type": "SetStreamSettings" , 	"message-id" : "SetStreamSettings"};
var save_stream_setting 	= {"is_send": 0, "request-type": "SaveStreamSettings", "message-id": "SaveStreamSettings"};

var get_transition_list		= {"is_send": 0, "request-type": "GetTransitionList" , 	"message-id" : "GetTransitionList"};
var get_current_transition	= {"is_send": 0, "request-type": "GetCurrentTransition" , "message-id" : "GetCurrentTransition"};
var set_current_transition 	= {"is_send": 0, "request-type": "SetCurrentTransition" , "message-id" : "SetCurrentTransition"};
var get_transition_duration = {"is_send": 0, "request-type": "SetTransitionDuration", "message-id" : "SetTransitionDuration"};
var set_volumn 				= {"is_send": 0, "request-type": "SetVolume" , 			"message-id" : "SetVolume"};
var get_volumn 				= {"is_send": 0, "request-type": "GetVolume" , 			"message-id" : "GetVolume", "source" : ""};
var set_mute 				= {"is_send": 0, "request-type": "SetMute" , 				"message-id" : "SetMute"};
var set_toggle_mute 		= {"is_send": 0, "request-type": "ToggleMute" , 			"message-id" : "ToggleMute"};
var set_current_scene_collection = {"is_send": 0, "request-type": "SetCurrentSceneCollection", 	"message-id" : "SetCurrentSceneCollection", "sc-name":""};
var get_current_scene_collection = {"is_send": 0, "request-type": "GetCurrentSceneCollection", 	"message-id" : "GetCurrentSceneCollection"};
var list_scene_collection 	= {"is_send": 0, "request-type": "ListSceneCollections", 	"message-id" : "ListSceneCollections"};
var set_current_profile 	= {"is_send": 0, "request-type": "SetCurrentProfile", 	"message-id" : "SetCurrentProfile", "profile-name":""};
var get_current_profile 	= {"is_send": 0, "request-type": "GetCurrentProfile", 	"message-id" : "GetCurrentProfile"};
var list_profile 			= {"is_send": 0, "request-type": "ListProfiles", 			"message-id" : "ListProfiles"};


function get_websocket_room(websocket)
{
	if(websocket.url === wsocket_green_url)
	{
		return GREEN_ROOM;
	}
	else if(websocket.url === wsocket_chaiklang_url)
	{
		return CHAIKLANG_ROOM;
	}
	else if(websocket.url === wsocket_violet_url)
	{
		return VIOLET_ROOM;
	}
	else if(websocket.url === wsocket_green_extend_url)
	{
		return GREEN_ROOM_EXTEND;
	}
	else
	{
		return -1;
	}
}
function get_websocket_by_sock_name(sock_name)
{
	if(sock_name === GREEN_ROOM_NAME)
	{
		return websocket_green();
	}
	else if(sock_name === CHAIKLANG_ROOM_NAME)
	{
		return websocket_chaiklang();
	}
	else if(sock_name === VIOLET_ROOM_NAME)
	{
		return websocket_violet();
	}
	else if(sock_name === GREEN_ROOM_EXTEND_NAME)
	{
		return websocket_green_extend();
	}
	else
	{
		return null;
	}
}

function enable_teacher_dropdown_baseon_cam_status()
{
	websocket_grn = get_websocket_by_sock_name(GREEN_ROOM_NAME);
	websocket_ckl = get_websocket_by_sock_name(CHAIKLANG_ROOM_NAME);
	websocket_violet_sock = get_websocket_by_sock_name(VIOLET_ROOM_NAME);

	// websocket.__is_cam_ready = true;
	if (websocket_grn.__is_cam_ready === true || 
		websocket_ckl.__is_cam_ready === true ||
		websocket_violet_sock.__is_cam_ready === true )
	{
		enable_teacher_and_subject();
	}
	else
	{
		disable_teacher_and_subject();
	}
}
function handleWebsocketUpdate(sock_name, json_update)
{
	websocket = get_websocket_by_sock_name(sock_name);
	switch(json_update["update-type"])
	{
		case "StreamStatus":
			if(json_update.streaming == true)
			{
				time = json_update["stream-timecode"].split('.')[0];
				websocket.stream_status_btn.children("#stream_time").text(time);
				websocket.stream_status_btn.removeClass('btn-default').addClass('btn-danger');
			}
			else if(json_update.streaming == false)
			{
				websocket.stream_status_btn.children("#stream_time").text("");
				websocket.stream_status_btn.removeClass('btn-danger').addClass('btn-default');
			}

			break;
		default:
			break;
	}
}
function handleWebsocketResponse(sock_name, json_resp)
{
	websocket = get_websocket_by_sock_name(sock_name);
	switch(json_resp["message-id"])
	{
		case set_current_profile["message-id"]:
			if(json_resp.status === "ok")
			{
				in_q = websocket.queue.peek();
				if(in_q["message-id"] == json_resp["message-id"])
				{
					// pop Q
					message = websocket.queue.dequeue();
				}
			}
			break;
		case get_current_profile["message-id"]:
			if(json_resp.status === "ok")
			{
				in_q = websocket.queue.peek();
				if(in_q["message-id"] == json_resp["message-id"])
				{
					// pop Q
					message = websocket.queue.dequeue();
				}
				if(websocket.__is_room_recording)
				{
					profile = json_resp["profile-name"].split("_");
					websocket.rec_status_btn.children("p.duration").text(profile[0] + " " + profile[1]);
				}
				else
				{
					websocket.rec_status_btn.children("p.duration").text("-");
				}
			}
			break;
		case set_current_scene_collection["message-id"]:
		case "open_Green_room_collection":
		case "open_Green_room_Unseen_collection":
		case "open_Chaiklang_room_collection":
		case "open_Chaiklang_room_Unseen_collection":
		case "open_Violet_room_collection":
		case "open_Violet_room_Unseen_collection":
		case "open_Green_room_extend_collection":
		case "open_Green_room_extend_Unseen_collection":
		case "open_Green_room_Live_stream_collection":
			if(json_resp.status === "ok")
			{
				in_q = websocket.queue.peek();
				if(in_q["message-id"] == json_resp["message-id"])
				{
					// pop Q
					message = websocket.queue.dequeue();
				}
			}
			break;
		case get_current_scene_collection["message-id"]:
			if(json_resp.status === "ok")
			{
				in_q = websocket.queue.peek();
				if(in_q["message-id"] == json_resp["message-id"])
				{
					// pop Q
					message = websocket.queue.dequeue();
				}
				
				if(json_resp["sc-name"] == WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)].trim())
				{
					websocket.cam_status_btn.find("p.status_text").html("สถานะกล้อง : ปกติ");
					websocket.cam_status_btn.removeClass('btn-default').addClass('btn-success');
					// $("#live_control").hide();
					websocket.__is_cam_ready = true;
				}
				else
				{
					websocket.cam_status_btn.find("p.status_text").html("<span style='color:red;'>สัญญาณภาพมีปัญหา<br>กดรีเซ็ตด้านล่าง</span>");
					websocket.cam_status_btn.removeClass('btn-success').addClass('btn-default');
					$("#live_control").slideDown();
					websocket.__is_cam_ready = false;
				}
			}
			break;
		case start_record["message-id"]:
			if(json_resp.status === "ok")
			{
				in_q = websocket.queue.peek();
				if(in_q["message-id"] == json_resp["message-id"])
				{
					// pop Q
					message = websocket.queue.dequeue();
					websocket.__is_room_recording = true;
				}
			}
			break;
		case stop_record["message-id"]:
			if(json_resp.status === "ok")
			{
				in_q = websocket.queue.peek();
				if(in_q["message-id"] == json_resp["message-id"])
				{
					// pop Q
					message = websocket.queue.dequeue();
					websocket.__is_room_recording = false;
				}
			}
			break;
		case get_stream_status["message-id"]:
			if(json_resp.status === "ok")
			{
				in_q = websocket.queue.peek();
				if(in_q["message-id"] == json_resp["message-id"])
				{
					// pop Q
					message = websocket.queue.dequeue();
				}
				if(json_resp.recording == true)
				{
					websocket.rec_status_btn.removeClass('btn-default').addClass('btn-danger');
					websocket.__is_room_recording = true;
				}
				else if(json_resp.recording == false)
				{
					websocket.rec_status_btn.removeClass('btn-danger').addClass('btn-default');
					websocket.rec_status_btn.children("p.duration").text("-");
					websocket.__is_room_recording = false;
				}
				if(get_websocket_room(websocket) == GREEN_ROOM)
				{
					if(json_resp.streaming == true)
					{
						$("#green_stream_status_btn").removeClass('btn-default').addClass('btn-danger');
						websocket.__is_room_streaming = true;
					}
					else if(json_resp.streaming == false)
					{
						$("#green_stream_status_btn").removeClass('btn-danger').addClass('btn-default');
						$("#green_stream_status_btn #stream_time").text("");
						websocket.__is_room_streaming = false;
					}
				}
			}
			break;
		case get_stream_setting["message-id"]:
			in_q = websocket.queue.peek();
			if(in_q["message-id"] == json_resp["message-id"])
			{
				// pop Q
				message = websocket.queue.dequeue();
			}
			break;
		case set_stream_setting["message-id"]:
			if(json_resp.status === "ok")
			{
				in_q = websocket.queue.peek();
				if(in_q["message-id"] == json_resp["message-id"])
				{
					// pop Q
					message = websocket.queue.dequeue();
				}
			}
			break;
		default:
			break;

	}
	setTimeout(enable_teacher_dropdown_baseon_cam_status, 200);
	return;
}
function websocket_is_open(websocket_obj) {
	if (websocket_obj.readyState !== STATE_OPEN) {
		return false;
	}
	else {
		return true;
	}
}
function onOpen(sock_name, sock_event)
{
	websocket = get_websocket_by_sock_name(sock_name);
	setInterval(send_Q_peek, 400 , sock_name);

	console.log(websocket);
	// sock_name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	console.log("("+sock_name+") CONNECTED");
	if (!websocket_is_open(websocket)) {
		// set btn text
		console.log(sock_name + " NOT OPEN @onOpen");
		websocket.cam_status_btn.find("p.status_text").html("<span style='color:red;'>ยังไม่เปิดโปรแกรม OBS</span>");
		websocket.cam_status_btn.removeClass('btn-success').addClass('btn-default');
	}
	else {
		console.log(sock_name + " OPEN @onOpen");
		websocket.cam_status_btn.find("p.status_text").html("สถานะกล้อง : ปกติ");
	}
	status_monitor(sock_name);
}
function onClose(sock_name, sock_event)
{
	// websocket = get_websocket_by_sock_name(sock_name);
	// sock_name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	// console.log("("+sock_name+") DISCONNECTED");
	setTimeout(initWebSocket, 5000);
}
function onMessage(sock_name, sock_event)
{
	websocket = get_websocket_by_sock_name(sock_name);
	var resp = jQuery.parseJSON(sock_event.data);
	// sock_name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	if(resp["message-id"] == null)
	{		
		if(iq_debug)console.log("UPDATE   ("+sock_name+") : " + sock_event.data.replace(/(\r\n|\n|\r)/g,""));
		handleWebsocketUpdate(sock_name, resp);

	}
	else
	{
		if(iq_debug)console.log("RESPONSE ("+sock_name+") : " + sock_event.data.replace(/(\r\n|\n|\r)/g,""));
		handleWebsocketResponse(sock_name, resp);
	}
}
function onError(sock_name, sock_event)
{
	websocket = get_websocket_by_sock_name(sock_name);
	// sock_name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	// console.log( "Error ("+sock_name+") : " + sock_event.data);
	// if (sock_name === GREEN_ROOM_NAME){
	// 	websocket = websocket_green_obj;
	// }
	// else if (sock_name === CHAIKLANG_ROOM_NAME) {
	// 	websocket = websocket_chaiklang_obj;
	// }
	// else if (sock_name === VIOLET_ROOM_NAME) {
	// 	websocket = websocket_violet_obj;
	// }
	if (!websocket_is_open(websocket)) {
		// set btn text
		console.log(sock_name + " NOT OPEN @onError");
		websocket.cam_status_btn.find("p.status_text").html("<span style='color:red;'>ยังไม่เปิดโปรแกรม OBS</span>");
		websocket.cam_status_btn.removeClass('btn-success').addClass('btn-default');
	}
	else {
		console.log(sock_name + " OPEN @onError");
		websocket.cam_status_btn.find("p.status_text").html("สถานะกล้อง : ปกติ");
	}
	setTimeout(initWebSocket, 5000);
}
function websocket_green()
{
	if(websocket_green_obj == null || 
		websocket_green_obj.readyState === STATE_CLOSED || 
		websocket_green_obj.readyState === STATE_CLOSING)
	{
		websocket_green_obj = new WebSocket(wsocket_green_url);
		websocket_green_obj.onopen = function(sock_event) { onOpen(GREEN_ROOM_NAME, sock_event) };
		websocket_green_obj.onclose = function(sock_event) { onClose(GREEN_ROOM_NAME, sock_event) };
		websocket_green_obj.onmessage = function(sock_event) { onMessage(GREEN_ROOM_NAME, sock_event) };
		websocket_green_obj.onerror = function(sock_event) { onError(GREEN_ROOM_NAME, sock_event) };
		websocket_green_obj.queue = new Queue();
		websocket_green_obj.cam_status_btn = $("#grn_camera_status_btn");
		websocket_green_obj.rec_status_btn = $("#grn_rec_status_btn");
		websocket_green_obj.stream_status_btn = $("#green_stream_status_btn");
		websocket_green_obj.__is_cam_ready = false;
		websocket_green_obj.__is_room_recording = false;
		websocket_green_obj.__is_room_streaming = false;
	}
	return websocket_green_obj;
}
function websocket_green_extend()
{
	if(websocket_green_extend_obj == null || 
		websocket_green_extend_obj.readyState === STATE_CLOSED || 
		websocket_green_extend_obj.readyState === STATE_CLOSING)
	{
		websocket_green_extend_obj = new WebSocket(wsocket_green_extend_url);
		websocket_green_extend_obj.onopen = function(sock_event) { onOpen(GREEN_ROOM_EXTEND_NAME, sock_event) };
		websocket_green_extend_obj.onclose = function(sock_event) { onClose(GREEN_ROOM_EXTEND_NAME, sock_event) };
		websocket_green_extend_obj.onmessage = function(sock_event) { onMessage(GREEN_ROOM_EXTEND_NAME, sock_event) };
		websocket_green_extend_obj.onerror = function(sock_event) { onError(GREEN_ROOM_EXTEND_NAME, sock_event) };
		websocket_green_extend_obj.queue = new Queue();
		websocket_green_extend_obj.cam_status_btn = $("#grn_camera_extend_status_btn");
		websocket_green_extend_obj.rec_status_btn = $("#grn_rec_extend_status_btn");
		websocket_green_extend_obj.stream_status_btn = null;
		websocket_green_extend_obj.__is_cam_ready = false;
		websocket_green_extend_obj.__is_room_recording = false;
		websocket_green_extend_obj.__is_room_streaming = false;
	}
	return websocket_green_extend_obj;
}
function websocket_chaiklang()
{
	if(websocket_chaiklang_obj == null || 
		websocket_chaiklang_obj.readyState === STATE_CLOSED || 
		websocket_chaiklang_obj.readyState === STATE_CLOSING)
	{
		websocket_chaiklang_obj = new WebSocket(wsocket_chaiklang_url);
		websocket_chaiklang_obj.onopen = function(sock_event) { onOpen(CHAIKLANG_ROOM_NAME, sock_event) };
		websocket_chaiklang_obj.onclose = function(sock_event) { onClose(CHAIKLANG_ROOM_NAME, sock_event) };
		websocket_chaiklang_obj.onmessage = function(sock_event) { onMessage(CHAIKLANG_ROOM_NAME, sock_event) };
		websocket_chaiklang_obj.onerror = function(sock_event) { onError(CHAIKLANG_ROOM_NAME, sock_event) };
		websocket_chaiklang_obj.queue = new Queue();
		websocket_chaiklang_obj.cam_status_btn = $("#chaiklang_camera_status_btn");
		websocket_chaiklang_obj.rec_status_btn = $("#chaiklang_rec_status_btn");
		websocket_chaiklang_obj.__is_cam_ready = false;
		websocket_chaiklang_obj.__is_room_recording = false;
		websocket_chaiklang_obj.__is_room_streaming = false;
	}
	return websocket_chaiklang_obj;
}
function websocket_violet()
{
	if(websocket_violet_obj == null || 
		websocket_violet_obj.readyState === STATE_CLOSED || 
		websocket_violet_obj.readyState === STATE_CLOSING)
	{
		websocket_violet_obj = new WebSocket(wsocket_violet_url);
		websocket_violet_obj.onopen = function(sock_event) { onOpen(VIOLET_ROOM_NAME, sock_event) };
		websocket_violet_obj.onclose = function(sock_event) { onClose(VIOLET_ROOM_NAME, sock_event) };
		websocket_violet_obj.onmessage = function(sock_event) { onMessage(VIOLET_ROOM_NAME, sock_event) };
		websocket_violet_obj.onerror = function(sock_event) { onError(VIOLET_ROOM_NAME, sock_event) };
		websocket_violet_obj.queue = new Queue();
		websocket_violet_obj.cam_status_btn = $("#violet_camera_status_btn");
		websocket_violet_obj.rec_status_btn = $("#violet_rec_status_btn");
		websocket_violet_obj.stream_status_btn = $("#violet_stream_status_btn");
		websocket_violet_obj.__is_cam_ready = false;
		websocket_violet_obj.__is_room_recording = false;
		websocket_violet_obj.__is_room_streaming = false;
	}
	return websocket_violet_obj;
}
function initWebSocket()
{
	websocket_green();
	// websocket_chaiklang();
	// websocket_violet();
	// websocket_green_extend();
	setTimeout(websocket_chaiklang, 500);
	setTimeout(websocket_violet, 1000);
	setTimeout(websocket_green_extend, 2000);
}

function doSend(sock_name, message)
{

	websocket = get_websocket_by_sock_name(sock_name);
	if(message === undefined)
	{
		console.log("message undefined");
		return;
	}
	// sock_name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)];
	if (!websocket_is_open(websocket))
	{
		if(iq_debug){console.log("WEBSOCKET ("+sock_name+") state "+websocket.readyState+" NOT OPEN @doSend");}
		initWebSocket();
		websocket.cam_status_btn.removeClass('btn-success').addClass('btn-default');
		websocket.__is_cam_ready = false;
		return;
	}
	
	if(message.is_send == 0)
	{
		if(iq_debug)console.log("Send     ("+sock_name+") : " + JSON.stringify(message));
		websocket.send(JSON.stringify(message));
		message.is_send = 1;
	}
	else
	{
		if(message.is_send++ == 3)
		{
			// resend
			websocket.send(JSON.stringify(message));
		}
		else if(message.is_send >= 6)
		{
			// Pop Q
			pop_message = websocket.queue.dequeue();
			if(iq_debug)console.log("Timeout     ("+sock_name+") : " + JSON.stringify(pop_message));
		}
	}
}

function send_Q_peek(sock_name)
{
	websocket = get_websocket_by_sock_name(sock_name);
	if(websocket.queue.getLength() > 0)
	{
		// sock_name = WEBSOCKET_ROOM_NAME[get_websocket_room(websocket)].trim();
		doSend(sock_name, websocket.queue.peek());
	}
}
function refresh_status(sock_name)
{
	websocket = get_websocket_by_sock_name(sock_name);
	datasend = jQuery.extend({}, get_current_scene_collection);
	websocket.queue.enqueue(datasend);
	
	datasend = jQuery.extend({}, get_stream_status);
	websocket.queue.enqueue(datasend);

	datasend = jQuery.extend({}, get_current_profile);
	websocket.queue.enqueue(datasend);

	if(get_websocket_room(websocket) == GREEN_ROOM)
	{
		datasend = jQuery.extend({}, get_stream_setting);
		websocket.queue.enqueue(datasend);
	}
	if(iq_debug)console.log("Q length : "+ websocket.queue.getLength());
}
function status_monitor(sock_name)
{
	refresh_status(sock_name);
	setTimeout(status_monitor, 10000, sock_name); // repeat myself
}
function enable_teacher_and_subject()
{
	$("select#teacher").prop('disabled', false);
	$("select#subject").prop('disabled', false);
}
function disable_teacher_and_subject() 
{
	$("select#teacher").prop('disabled', true);
	$("select#subject").prop('disabled', true);
}
function reset_teacher_and_subject()
{
	Key = "not_select";
	Value = "เลือกวิชา:---"; 
	option = new Option(Value, Key); 
	$("select#subject").empty().append($(option));
	$("select#teacher").val("not_select").change();

	disable_record_btn(GREEN_ROOM_NAME);
	disable_record_btn(CHAIKLANG_ROOM_NAME);
	disable_record_btn(VIOLET_ROOM_NAME);
}
function enable_record_btn(room_name) {
	btn_prefix = "";
	websocket = get_websocket_by_sock_name(room_name);
	if ((websocket.__is_cam_ready === false) || (websocket.__is_room_recording === true))
	{
		return;
	}

	if (room_name === GREEN_ROOM_NAME)
	{
		btn_prefix = "green";
	}
	else if (room_name === CHAIKLANG_ROOM_NAME) {
		btn_prefix = "chaiklang";
	}
	else if (room_name === VIOLET_ROOM_NAME) {
		btn_prefix = "violet";
	}
	else if (room_name === GREEN_ROOM_EXTEND_NAME) {
		btn_prefix = "green_extend";
	}
	$("button#" + btn_prefix + "_startrecord_btn").prop('disabled', false).removeClass("btn-default").addClass("btn-primary");
}
function disable_record_btn(room_name) {
	btn_prefix = "";
	// websocket = get_websocket_by_sock_name(room_name);
	// if (websocket.__is_cam_ready === true) {
	// 	return;
	// }
	if (room_name === GREEN_ROOM_NAME) {
		btn_prefix = "green";
	}
	else if (room_name === CHAIKLANG_ROOM_NAME) {
		btn_prefix = "chaiklang";
	}
	else if (room_name === VIOLET_ROOM_NAME) {
		btn_prefix = "violet";
	}
	else if (room_name === GREEN_ROOM_EXTEND_NAME) {
		btn_prefix = "green_extend";
	}
	$("button#" + btn_prefix + "_startrecord_btn").prop('disabled', true).removeClass("btn-primary").addClass("btn-default");
}
function scene_unseen_then_green()
{
	//////////////////
	// Open Unseen collection 
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Green_room_Unseen_collection";
	datasend["sc-name"] = "Unseen";
	websocket_green().queue.enqueue(datasend);


	//////////////////
	// Open Green_room collection
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Green_room_collection";
	datasend["sc-name"] = GREEN_ROOM_SCENE_COLLECTION_NAME;
	websocket_green().queue.enqueue(datasend);
}
function scene_unseen_then_chaiklang()
{
	//////////////////
	// Open Unseen collection 
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Chaiklang_room_Unseen_collection";
	datasend["sc-name"] = "Unseen";
	websocket_chaiklang().queue.enqueue(datasend);


	//////////////////
	// Open Chaiklang_room collection
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Chaiklang_room_collection";
	datasend["sc-name"] = CHAIKLANG_ROOM_SCENE_COLLECTION_NAME; 
	websocket_chaiklang().queue.enqueue(datasend);
}
function scene_unseen_then_violet()
{
	//////////////////
	// Open Unseen collection 
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Violet_room_Unseen_collection";
	datasend["sc-name"] = "Unseen";
	websocket_violet().queue.enqueue(datasend);


	//////////////////
	// Open Violet_room collection
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Violet_room_collection";
	datasend["sc-name"] = VIOLET_ROOM_SCENE_COLLECTION_NAME; 
	websocket_violet().queue.enqueue(datasend);
}
function scene_unseen_then_green_extend()
{
	//////////////////
	// Open Unseen collection 
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Green_room_extend_Unseen_collection";
	datasend["sc-name"] = "Unseen";
	websocket_green_extend().queue.enqueue(datasend);


	//////////////////
	// Open Green_room collection
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Green_room_extend_collection";
	datasend["sc-name"] = GREEN_ROOM_EXTEND_SCENE_COLLECTION_NAME;
	websocket_green_extend().queue.enqueue(datasend);
	return 0;
}
function scene_unseen_then_green_facebooklive_whiteboard() {
	//////////////////
	// Open Unseen collection 
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Green_room_Unseen_collection";
	datasend["sc-name"] = "Unseen";
	websocket_green().queue.enqueue(datasend);


	//////////////////
	// Open Chaiklang_room collection
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Green_room_Live_stream_collection";
	datasend["sc-name"] = GREEN_ROOM_FB_LIVE_WHITEBOARD_SCENE_COLLECTION_NAME;
	websocket_green().queue.enqueue(datasend);
}
function scene_unseen_then_green_facebooklive_tablet() {
	//////////////////
	// Open Unseen collection 
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Green_room_Unseen_collection";
	datasend["sc-name"] = "Unseen";
	websocket_green().queue.enqueue(datasend);


	//////////////////
	// Open Chaiklang_room collection
	datasend = jQuery.extend({}, set_current_scene_collection);
	datasend["message-id"] = "open_Green_room_Live_stream_collection";
	datasend["sc-name"] = GREEN_ROOM_FB_LIVE_TABLET_SCENE_COLLECTION_NAME;
	websocket_green().queue.enqueue(datasend);
}
function camera_reset(room_name){
	websocket = get_websocket_by_sock_name(room_name);
	if ((websocket.__is_cam_ready === true) || (websocket.__is_room_recording === true)) {
		console.log("Cam ready | Recording")
		return;
	}
	if(room_name == GREEN_ROOM_NAME)
	{
		scene_unseen_then_green();
	}
	else if(room_name == CHAIKLANG_ROOM_NAME)
	{
		scene_unseen_then_chaiklang();
	}
	else if(room_name == VIOLET_ROOM_NAME)
	{
		scene_unseen_then_violet();
	}
}
var blink_on = false;
var blink_tid = 0;
var last_level = "";
function blink_red_background()
{
	if(blink_on){
		$("body").css("background-color", "#5d5d5d");
		blink_on = false;
	}
	else{
		$("body").css("background-color", "#FF3639");
		blink_on = true;
	}
}
function blink_yellow_background()
{
	//FFE85C
	if(blink_on){
		$("body").css("background-color", "#5d5d5d");
		blink_on = false;
	}
	else{
		$("body").css("background-color", "#FFE85C");
		blink_on = true;
	}
}
function blink_clear_background()
{
	$("body").css("background-color", "#5d5d5d");
	blink_on = false;
}
function check_disk_space()
{
	param = { "disk_name" : ["C:", "D:"]};
	get = $.getJSON("/vdo_control/vdo_record_tool/check_disk_space.php", param, function(data, textStatus, jqXHR){
		disk_status = data.disk_status;
		if(disk_status.event_level == "critical")
		{
			// set red blink
			if(blink_tid != 0 && last_level != disk_status.event_level){
				// change level
				//clear last tid
				clearInterval(blink_tid);
				blink_tid = setInterval(blink_red_background, 500);
			}
			else if(blink_tid == 0)
			{
				// last_level is normal
				blink_tid = setInterval(blink_red_background, 500);
			}
		}
		else if(disk_status.event_level == "warning")
		{
			// console.log("SET YELLOW");
			// set yellow
			if(blink_tid != 0 && last_level != disk_status.event_level){
				// change level
				//clear last tid
				clearInterval(blink_tid);
				blink_tid = setInterval(blink_yellow_background, 1000);
			}
			else if(blink_tid == 0)
			{
				// last_level is normal
				blink_tid = setInterval(blink_yellow_background, 1000);
			}
		}
		else if(disk_status.event_level == "normal")
		{
			// clear color
			clearInterval(blink_tid);
			blink_clear_background();
		}
		last_level = disk_status.event_level;
	});
	get.fail(function(jqXHR, textStatus, errorThrown) {
		// $( "#result" ).html( "error" );
		console.log(jqXHR.responseText);
		console.log(textStatus);
	});
}
function init_check_disk_space()
{
	check_disk_space();
	var disk_space_tid = setInterval(check_disk_space, 60 * 1000);
}
jQuery( document ).ready(function($) {
	console.log("START INIT PROCESS");
	$('#green_startrecord_btn').on('click', function() {
		if(websocket_green().__is_room_recording)
		{
			return;
		}
		if($("select#subject").val() === "not_select")
		{
			waitingDialog.show("เลือกวิชาก่อนจ้า", 2000);
			return;
		}
		//////////////////
		// change profile
		profile_name = $("select#subject").val()+"_green_profile";
		// clone object
		datasend = jQuery.extend({}, set_current_profile);
		datasend["profile-name"] = profile_name;
		// add to Q
		websocket_green().queue.enqueue(datasend);
		websocket_green_extend().queue.enqueue(datasend);


		// //////////////////
		// // Open Unseen collection 
		// datasend = jQuery.extend({}, set_current_scene_collection);
		// datasend["message-id"] = "open_Green_room_Unseen_collection";
		// datasend["sc-name"] = "Unseen";
		// websocket_green().queue.enqueue(datasend);


		// //////////////////
		// // Open Green_room collection
		// datasend = jQuery.extend({}, set_current_scene_collection);
		// datasend["message-id"] = "open_Green_room_collection";
		// datasend["sc-name"] = "Green_room";
		// websocket_green().queue.enqueue(datasend);
		scene_unseen_then_green();

		datasend = jQuery.extend({}, start_record);
		websocket_green().queue.enqueue(datasend);
		add_log("green_startrecord_btn", "Enqueue " + profile_name);
		reset_teacher_and_subject();
		refresh_status(GREEN_ROOM_NAME);

		scene_unseen_then_green_extend();
		datasend = jQuery.extend({}, start_record);
		websocket_green_extend().queue.enqueue(datasend);
		refresh_status(GREEN_ROOM_EXTEND_NAME);
		add_log("green_startrecord_btn set Extend record", "Enqueue " + profile_name);
	});
	$('#green_stoprecord_btn').on('click', function() {
		if(websocket_green().__is_room_recording)
		{
			bootbox.confirm({
				message: '<h3 style="margin:0;">หยุดอัดห้องเขียว ?<!--<br>กด yes แล้วรอ 10 วิค่อยปิดมือถือ--></h3>',
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success btn-wide'
					},
					cancel: {
						label: ' No ',
						className: 'btn-danger btn-wide'
					}
				},
				callback: function (result) {
					if(result == true)
					{
						datasend = jQuery.extend({}, stop_record);
						websocket_green().queue.enqueue(datasend);
						add_log("green_stoprecord_btn", "Enqueue");
						reset_teacher_and_subject();
						refresh_status(GREEN_ROOM_NAME);

						datasend = jQuery.extend({}, stop_record);
						websocket_green_extend().queue.enqueue(datasend);
						refresh_status(GREEN_ROOM_EXTEND_NAME);
					}
				}
			});
		}
	});
	$('#chaiklang_startrecord_btn').on('click', function() {
		if(websocket_chaiklang().__is_room_recording)
		{
			return;
		}
		if($("select#subject").val() === "not_select")
		{
			waitingDialog.show("เลือกวิชาก่อนจ้า", 2000);
			return;
		}
		//////////////////
		// change profile
		profile_name = $("select#subject").val()+"_chaiklang_profile";
		// clone object
		datasend = jQuery.extend({}, set_current_profile);
		datasend["profile-name"] = profile_name;
		// add to Q
		websocket_chaiklang().queue.enqueue(datasend);


		// //////////////////
		// // Open Unseen collection 
		// datasend = jQuery.extend({}, set_current_scene_collection);
		// datasend["message-id"] = "open_Chaiklang_room_Unseen_collection";
		// datasend["sc-name"] = "Unseen";
		// websocket_chaiklang().queue.enqueue(datasend);


		// //////////////////
		// // Open Chaiklang_room collection
		// datasend = jQuery.extend({}, set_current_scene_collection);
		// datasend["message-id"] = "open_Chaiklang_room_collection";
		// datasend["sc-name"] = "Chaiklang_room";
		// websocket_chaiklang().queue.enqueue(datasend);
		scene_unseen_then_chaiklang();

		datasend = jQuery.extend({}, start_record);
		websocket_chaiklang().queue.enqueue(datasend);

		add_log("chaiklang_startrecord_btn", "Enqueue " + profile_name);
		reset_teacher_and_subject();
		refresh_status(CHAIKLANG_ROOM_NAME);
	});
	$('#chaiklang_stoprecord_btn').on('click', function() {
		if(websocket_chaiklang().__is_room_recording)
		{
			bootbox.confirm({
				message: '<h3 style="margin:0;">หยุดอัดห้องชายกลาง ?<!--<br>กด yes แล้วรอ 10 วิค่อยปิดมือถือ--></h3>',
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success btn-wide'
					},
					cancel: {
						label: ' No ',
						className: 'btn-danger btn-wide'
					}
				},
				callback: function (result) {
					if(result == true)
					{
						datasend = jQuery.extend({}, stop_record);
						websocket_chaiklang().queue.enqueue(datasend);
						add_log("chaiklang_stoprecord_btn", "Enqueue");
						reset_teacher_and_subject();
						refresh_status(CHAIKLANG_ROOM_NAME);
					}
				}
			});
		}
	});
	$('#violet_startrecord_btn').on('click', function() {
		if(websocket_violet().__is_room_recording)
		{
			return;
		}
		if($("select#subject").val() === "not_select")
		{
			waitingDialog.show("เลือกวิชาก่อนจ้า", 2000);
			return;
		}
		//////////////////
		// change profile
		profile_name = $("select#subject").val()+"_violet_profile";
		// clone object
		datasend = jQuery.extend({}, set_current_profile);
		datasend["profile-name"] = profile_name;
		// add to Q
		websocket_violet().queue.enqueue(datasend);

		scene_unseen_then_violet();

		datasend = jQuery.extend({}, start_record);
		websocket_violet().queue.enqueue(datasend);

		add_log("violet_startrecord_btn", "Enqueue " + profile_name);
		reset_teacher_and_subject();
		refresh_status(VIOLET_ROOM_NAME);
	});
	$('#violet_stoprecord_btn').on('click', function() {
		if(websocket_violet().__is_room_recording)
		{
			bootbox.confirm({
				message: '<h3 style="margin:0;">หยุดอัดห้องม่วง ?<!--<br>กด yes แล้วรอ 10 วิค่อยปิดมือถือ--></h3>',
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success btn-wide'
					},
					cancel: {
						label: ' No ',
						className: 'btn-danger btn-wide'
					}
				},
				callback: function (result) {
					if(result == true)
					{
						datasend = jQuery.extend({}, stop_record);
						websocket_violet().queue.enqueue(datasend);
						add_log("violet_stoprecord_btn", "Enqueue");
						reset_teacher_and_subject();
						refresh_status(VIOLET_ROOM_NAME);
					}
				}
			});
		}
	});

	$("#load_fb_live_scene").click(function () {	
		whiteboard_or_tablet = $("input[name='fb_live_whiteboard_or_tablet']:checked").val();
		if (whiteboard_or_tablet == "whiteboard") {
			scene_unseen_then_green_facebooklive_whiteboard();
		}
		else {
			scene_unseen_then_green_facebooklive_tablet();
		}
		// change profile
		profile_name = "FacebookLive";
		// clone object
		datasend = jQuery.extend({}, set_current_profile);
		datasend["profile-name"] = profile_name;
		// add to Q
		websocket_green().queue.enqueue(datasend);
	});
	$("#green_startstream_btn").click(function(){	
		if(!websocket_green().__is_room_streaming)
		{
			fb_live_key = $("#fb_live_key").val();
			whiteboard_or_tablet = $("input[name='fb_live_whiteboard_or_tablet']:checked").val();
			if(fb_live_key == null || fb_live_key == "")
			{
				var ret = 0;
				$.when(waitingDialog.show("ใส่ Facebook Live key จ้า", 1300))
				.then(function(){
					$("#fb_live_key").focus();

					return -1;
				});
			}
			else
			{
				exe_time = 0;
				// change profile
				profile_name = "FacebookLive";
				// clone object
				datasend = jQuery.extend({}, set_current_profile);
				datasend["profile-name"] = profile_name;
				// add to Q
				websocket_green().queue.enqueue(datasend);
				if (whiteboard_or_tablet == "whiteboard"){
					scene_unseen_then_green_facebooklive_whiteboard();
				}
				else{
					scene_unseen_then_green_facebooklive_tablet();
				}
				
				// exe_time += 200;
				// setTimeout(function(){
				// 	var dfd1 = jQuery.Deferred();
				// 	//////////////////
				// 	// Open Unseen collection 
				// 	datasend = jQuery.extend({}, set_current_scene_collection);
				// 	datasend["message-id"] = "open_Green_room_Unseen_collection";
				// 	datasend["sc-name"] = "Unseen";
				// 	websocket_green().queue.enqueue(datasend);
				// }, exe_time);

				// exe_time += 500;
				// setTimeout(function(){
				// 	//////////////////
				// 	// Open Green_room Live collection
				// 	datasend = jQuery.extend({}, set_current_scene_collection);
				// 	datasend["message-id"] = "open_Green_room_Live_stream_collection";
				// 	datasend["sc-name"] = GREEN_ROOM_LIVE_SCENE_COLLECTION_NAME;
				// 	websocket_green().queue.enqueue(datasend);

				// }, exe_time);

				exe_time += 300;
				setTimeout(function(){
					// start stream
					datasend = jQuery.extend({}, set_stream_setting);
					datasend.type = "rtmp_custom";
					datasend.settings = Object();
					datasend.settings.server = "rtmp://live-api.facebook.com:80/rtmp/"
					// datasend.settings.service = "Facebook Live";
					datasend.settings.key = fb_live_key;
					datasend.settings["use-auth"] = false;
					datasend.save = true;
					websocket_green().queue.enqueue(datasend);
					console.log(datasend);

					datasend = jQuery.extend({}, save_stream_setting);
					websocket_green().queue.enqueue(datasend);
				}, exe_time);

				exe_time += 500;
				setTimeout(function(){
					var dfd4 = jQuery.Deferred();
					datasend = jQuery.extend({}, start_stream);
					datasend.type = "rtmp_custom";
					datasend.settings = Object();
					datasend.settings.server = "rtmp://live-api.facebook.com:80/rtmp/";
					// datasend.settings.service = "Facebook Live";
					datasend.settings.key = fb_live_key;
					datasend.settings["use-auth"] = false;
					datasend.save = true;
					websocket_green().queue.enqueue(datasend);
					refresh_status(GREEN_ROOM_NAME);
				}, exe_time);
			}
		}
	});
	$("#green_stopstream_btn").click(function(){
		if(websocket_green().__is_room_streaming)
		{
			bootbox.confirm({
				message: '<h3 style="margin:0;">หยุด Live ?</h3>',
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success  btn-wide'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger  btn-wide'
					}
				},
				callback: function (result) {
					if(result == true)
					{
						datasend = jQuery.extend({}, stop_stream);
						datasend.type = "rtmp_custom";
						datasend.settings = Object();
						datasend.settings.server = "rtmp://live-api.facebook.com:80/rtmp/";
						datasend.settings.key = fb_live_key;
						datasend.settings["use-auth"] = false;
						datasend.save = true;
						websocket_green().queue.enqueue(datasend);
						add_log("green_stop stream btn", "Enqueue");
						refresh_status(GREEN_ROOM_NAME);
						// clear key input 
						// $("#fb_live_key").val("");
					}
				}
			});
		}
	});
	$("select#teacher").on('change', function() {
		subject_array = subject_by_teacher[this.value];
		
		if(subject_array != null)
		{
			$("select#subject").empty();
			$.each(subject_array, function() { 
				// this.Key and this.Value refer to array(key, value) of subject
				var option = new Option(this.Value, this.Key); 
				$("select#subject").append($(option));
			}); 
		}
	});
	$("select#subject").on('change', function () {
		if (this.value === "not_select")
		{
			disable_record_btn(GREEN_ROOM_NAME);
			disable_record_btn(CHAIKLANG_ROOM_NAME);
			disable_record_btn(VIOLET_ROOM_NAME);
		}
		else
		{
			enable_record_btn(GREEN_ROOM_NAME);
			enable_record_btn(CHAIKLANG_ROOM_NAME);
			enable_record_btn(VIOLET_ROOM_NAME);
		}
	});
	$("button#green_room_camera_reset_btn").click(function(){
		camera_reset(GREEN_ROOM_NAME);
		add_log("RESET CAM", GREEN_ROOM_NAME);
	});
	$("button#chaiklang_room_camera_reset_btn").click(function () {
		camera_reset(CHAIKLANG_ROOM_NAME);
		add_log("RESET CAM", CHAIKLANG_ROOM_NAME);
	});
	$("button#violet_room_camera_reset_btn").click(function () {
		camera_reset(VIOLET_ROOM_NAME);
		add_log("RESET CAM", VIOLET_ROOM_NAME);
	});
	$(".show_live_down").click(function(){
		$("#live_control").toggle();
	});
	initWebSocket();
	init_check_disk_space();
	console.log("BEGIN CONTROL");
	// $("select#teacher").val("fern");
	// $("select#teacher").change();
});
