var get_recording_dir = { "is_send": 0, "request-type": "GetRecordingFolder", "message-id": "GetRecordingFolder" };
var set_recording_dir = { "is_send": 0, "request-type": "SetRecordingFolder", "message-id": "SetRecordingFolder" };

var get_filename_format = { "is_send": 0, "request-type": "GetFilenameFormatting", "message-id": "GetFilenameFormatting" };
var set_filename_format = { "is_send": 0, "request-type": "SetFilenameFormatting", "message-id": "SetFilenameFormatting" };


var heartbeat = { "is_send": 0, "request-type": "Heartbeat", "message-id": "Heartbeat" };
var set_heartbeat = { "is_send": 0, "request-type": "SetHeartbeat", "message-id": "SetHeartbeat" };

function test_status(sock_name) {
    console.log("TEST : " + sock_name);
    websocket = get_websocket_by_sock_name(sock_name);

    // datasend = jQuery.extend({}, get_filename_format);
    // websocket.queue.enqueue(datasend);

    if (iq_debug) console.log("Q length : " + websocket.queue.getLength());
}