var fern_subject = [ 
	{Key:"not_select", Value:"เลือกวิชา:---"},
	{Key:"Math_p5",Value:"Math ป.5" }, 
	{Key:"Math_p6",Value:"Math ป.6" }, 
	{Key:"Math_m1",Value:"Math ม.1" }, 
	{Key:"Math_m2",Value:"Math ม.2" }, 
	{Key:"Math_m3",Value:"Math ม.3" }, 
	{Key:"Math_m3_intensive",Value:"Math ม.3 พสวท" }, 
	{Key:"Math_m4",Value:"Math ม.4" }, 
	{Key:"Math_m5",Value:"Math ม.5" }, 
	{Key:"Math_m6",Value:"Math ม.6" }
	]; 
var kong_subject = [ 
	{Key:"not_select", Value:"เลือกวิชา:---"},
	{Key:"Sci_p5",Value:"Sci ป.5" }, 
	{Key:"Sci_p6",Value:"Sci ป.6" }, 
	{Key:"Sci_m1",Value:"Sci ม.1" }, 
	{Key:"Sci_m2",Value:"Sci ม.2" }, 
	{Key:"Sci_m3",Value:"Sci ม.3" }, 
	{Key:"Sci_m3_intensive",Value:"Sci ม.3 พสวท" }, 
	{Key:"Phy_m4",Value:"Phy ม.4" }, 
	{Key:"Phy_m5",Value:"Phy ม.5" }, 
	{Key:"Phy_m6",Value:"Phy ม.6" }
	]; 
var tom_subject = [
	{Key:"not_select", Value:"เลือกวิชา:---"},
	{Key:"Math_p5",Value:"Math ป.5" },
	{Key:"Thai_p4",Value:"ไทย ป.4" }
];

var subject_by_teacher = [];
subject_by_teacher["fern"] 		= fern_subject;
subject_by_teacher["kong"] 		= kong_subject;
subject_by_teacher["tom"] 	= tom_subject;
